from tkinter import *

# Main Window Options
window = Tk()
window.title('Simple Calculator')
window.maxsize(320, 512)

# The Input Window Options and Placement
entry = Entry(window, width=35, borderwidth=5)
entry.grid(row=0, column=0, columnspan=3, rowspan=2, padx=10, pady=10)


# This gives the add button functionality
def button_add():
    global current
    global operator
    current = int(entry.get())
    entry.delete(0, END)
    operator = 1


# This gives the subtract button functionality
def button_sub():
    global current
    global operator
    current = int(entry.get())
    entry.delete(0, END)
    operator = 2


# This gives the multiply button functionality
def button_mul():
    global current
    global operator
    current = int(entry.get())
    entry.delete(0, END)
    operator = 3


# This gives the divide button functionality
def button_div():
    global current
    global operator
    current = int(entry.get())
    entry.delete(0, END)
    operator = 4


# This gives the equals button functionality
def button_equal():
    global current
    global operator
    global clear
    clear = 1
    if operator == 1:
        operator = 0
        current += int(entry.get())
        entry.delete(0, END)
        entry.insert(0, current)
    elif operator == 2:
        operator = 0
        current -= int(entry.get())
        entry.delete(0, END)
        entry.insert(0, current)
    elif operator == 3:
        operator = 0
        current *= int(entry.get())
        entry.delete(0, END)
        entry.insert(0, current)
    elif operator == 4:
        operator = 0
        current /= int(entry.get())
        entry.delete(0, END)
        entry.insert(0, current)
    elif operator == 0:
        entry.delete(0, END)
        entry.insert(0, 'Please input something first.')
    else:
        entry.delete(0, END)
        entry.insert(0, 'You broke my calculator :(')


# This gives the clear button functionality
def button_clear():
    global current
    entry.delete(0, END)
    current = 0


# This makes it possible to cleanly input numbers
def button_click(num):
    global clear
    if clear == 0:
        cur = entry.get()
        entry.delete(0, END)
        entry.insert(0, int(str(cur) + str(num)))
    else:
        entry.delete(0, END)
        entry.insert(0, num)
        clear = 0


# This stores the current value
current = 0
# Var determines if button_equal must add(1),subtract(2),multiply(3), divide(4), or do nothing(0)
operator = 0
# This tells button_click if it should clear entry box
clear = 0


# Buttons Defined
button1 = Button(window, text='1', padx=40, pady=20, command=lambda: button_click(1))
button2 = Button(window, text='2', padx=40, pady=20, command=lambda: button_click(2))
button3 = Button(window, text='3', padx=40, pady=20, command=lambda: button_click(3))
button4 = Button(window, text='4', padx=40, pady=20, command=lambda: button_click(4))
button5 = Button(window, text='5', padx=40, pady=20, command=lambda: button_click(5))
button6 = Button(window, text='6', padx=40, pady=20, command=lambda: button_click(6))
button7 = Button(window, text='7', padx=40, pady=20, command=lambda: button_click(7))
button8 = Button(window, text='8', padx=40, pady=20, command=lambda: button_click(8))
button9 = Button(window, text='9', padx=40, pady=20, command=lambda: button_click(9))
button0 = Button(window, text='0', padx=40, pady=20, command=lambda: button_click(0))
button_add = Button(window, text='+', padx=39, pady=20, command=button_add)
button_sub = Button(window, text='-', padx=42, pady=20, command=button_sub)
button_mul = Button(window, text='*', padx=40, pady=20, command=button_mul)
button_div = Button(window, text='/', padx=42, pady=20, command=button_div)
button_equal = Button(window, text='=', padx=140, pady=20, command=button_equal)
button_clear = Button(window, text='C', padx=40, pady=20, command=button_clear)
# button_decimal = Button(window, text='.', padx=79, pady=20, command=lambda: button_click('.'))


# Top Row of Buttons Number Placed on Grid
button7.grid(row=2, column=0)
button8.grid(row=2, column=1)
button9.grid(row=2, column=2)

# Middle Row of Number Buttons Placed on Grid
button1.grid(row=4, column=0)
button2.grid(row=4, column=1)
button3.grid(row=4, column=2)

# Bottom Row of Number Buttons Placed on Grid
button4.grid(row=3, column=0)
button5.grid(row=3, column=1)
button6.grid(row=3, column=2)

# First Line of Input Options Placed on Grid (Plus zero)
button0.grid(row=5, column=0)
button_add.grid(row=5, column=1)
button_sub.grid(row=5, column=2)

# Second Line of Input Options Placed on Grid
button_clear.grid(row=6, column=0)
button_mul.grid(row=6, column=1)
button_div.grid(row=6, column=2)

# Equal Button Placed on Grid, Column Span will be adjusted to make room for decimal, perhaps sci calculator button
button_equal.grid(row=7, column=0, columnspan=3)


# button_decimal.grid(row=7, column=3) #this is a feature that is being worked

# This runs the actual window
window.mainloop()
