Unu is a simple calculator written in python.

It is being created as a study into how to utilize tkinter to create GUIs.
This is so that I can create user friendly applications for the Pinephone and Librem 5.

Unu currently is capable of adding, subtracting, multiplying, and dividing.
However, it only updates upon hitting the equal button.

It runs as a python script and requires python3 as a dependency.
To install tkinter `sudo apt install python3-tk`
